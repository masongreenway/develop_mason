#pragma once
#include "SystemMessage.h"

class ISubSystem
{
public:

	virtual void Message(SystemMessage &msg) = 0;

};