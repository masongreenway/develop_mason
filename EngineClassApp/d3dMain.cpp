#include "d3dMain.h"
#include "DirectInput.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	// Enable Console
	AllocConsole();

	HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
	int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
	FILE* hf_out = _fdopen(hCrt, "w");
	setvbuf(hf_out, NULL, _IONBF, 1);
	*stdout = *hf_out;

	HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
	hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
	FILE* hf_in = _fdopen(hCrt, "r");
	setvbuf(hf_in, NULL, _IONBF, 128);
	*stdin = *hf_in;
	
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
		//_CrtSetBreakAlloc(542);
	#endif

	GameFramework app(hInstance, "Base DX9 Project", D3DDEVTYPE_HAL, D3DCREATE_HARDWARE_VERTEXPROCESSING);
	gd3dApp = &app;

	DirectInput di(DISCL_NONEXCLUSIVE|DISCL_FOREGROUND, DISCL_NONEXCLUSIVE|DISCL_FOREGROUND);
	gDInput = &di;
	
	return gd3dApp->run();
}

GameFramework::GameFramework(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP)
: D3DApp(hInstance, winCaption, devType, requestedVP)
{
	srand(time_t(0));

	if(!checkDeviceCaps())
	{
		MessageBox(0, "checkDeviceCaps() Failed", 0, 0);
		PostQuitMessage(0);
	}

	gXinput = new XBOXController(1);
	
	m_paused = false;
}

GameFramework::~GameFramework()
{
	delete gXinput;
	delete gDInput;
}

bool GameFramework::checkDeviceCaps()
{
	// Nothing to check.
	return true;
}

bool GameFramework::isPaused()
{
	return m_paused;
}

void GameFramework::SetPause(bool b)
{
	m_paused = b;
}

void GameFramework::TogglePause()
{
	if(m_paused)
	{
		m_paused = false;
	}
	else
	{
		m_paused = true;
	}
}

void GameFramework::onLostDevice()
{

}

void GameFramework::onResetDevice()
{

}

void GameFramework::startGSM()
{

}

void GameFramework::updateScene(float dt)
{
	//Poll Input
	EventXBOXController();
	gDInput->poll();


	if(!isPaused())
	{

	}
}

void GameFramework::drawScene()
{
	gd3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 60), 1.0f, 0);
	gd3dDevice->BeginScene();



	gd3dDevice->EndScene();
	gd3dDevice->Present(0, 0, 0, 0);
}

void GameFramework::EventXBOXController()
{
	lock.acquire();

	//check for controller pause
	if(gXinput->current[0].Start && (gXinput->previous[0].Start == false))
	{
		if(!m_paused)
		{
			m_paused = true;
		} else {
			m_paused = false;
		}
	}

	//check for fullscreen toggle
	if(gXinput->current[0].Select && (gXinput->previous[0].Select == false))
		enableFullScreenMode(md3dPP.Windowed);

	gXinput->wait = false;
	
	lock.release();
}

